<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php if (post_password_required()) { ?>
			<section class="page-section basic-content white-bg" id="section-0" role="section">
				<div class="container small">
					<div class="content-wrap">
						<?php echo get_the_password_form(); ?>
					</div><!-- end content-wrap -->	
				</div><!-- end container -->
			</section>
		<?php	
		}
		else {
			get_template_part('template-parts/partials/page-sections'); 
		}
		?>  
		
	<?php endwhile; endif; ?>

<?php get_footer(); ?>