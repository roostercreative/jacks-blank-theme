<?php get_header(); ?>

	<section class="page-section basic-content white-bg" role="section">
		<div class="container small clearfix">
			<div class="content-wrap">
				<p class="search-query">Search Results for "<?php echo get_search_query(); ?>"</p>

				<?php get_search_form(); ?>
				
				<div class="main-search-results" aria-label="search results">
					<?php 
					if (have_posts()) { 
						while (have_posts()) : the_post(); 
							$excerpt = get_field('the_excerpt');
						?>
							<div <?php post_class('search-result') ?> id="post-<?php the_ID(); ?>">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<?php if ($excerpt) { ?>
									<div class="excerpt">
										<?php echo $excerpt; ?>
									</div><!-- end excerpt -->
								<?php } ?>
								<a class="button" href="<?php the_permalink(); ?>">Read More</a>
							</div><!-- end post -->
						<?php 
						endwhile;
					}
					else { ?>
						<h3>No results found</h3>
						<p>Sorry no results were found for "<?php echo get_search_query(); ?>". Please try another search term(s).</p>
					<?php } ?>

					<div class="pagination clearfix" aria-label="pagination">
						<div class="prev">
							<?php next_posts_link('Previous Results'); ?>
						</div><!-- end prev -->
						<div class="next">
							<?php previous_posts_link('Next Results'); ?>
						</div><!-- end next -->
					</div><!-- end pagination -->
				</div><!-- end search-results -->
			</div><!-- end content-wrap -->	
		</div><!-- end container -->
	</section>

<?php get_footer(); ?>