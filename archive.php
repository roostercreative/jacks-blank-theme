<?php get_header(); ?>

	<section class="page-section news-posts four-columns" role="section" aria-label="Archive Posts">
		<div class="container">
			<?php //get_template_part('template-parts/partials/posts/news-cats'); ?>
			<div class="posts-container blocks-container wow fadeIn" data-wow-offset="200">
				<div class="posts-wrap grid-container">
					<?php
					if (have_posts()) {
						$i = 1;
						while (have_posts()) : the_post();
							include(locate_template('template-parts/partials/blocks/post-block.php'));
						$i++;
						endwhile;
					}
					else { ?>
						<h3>No Posts found.</h3>
					<?php
					}
					wp_reset_postdata();
					?>
				</div><!-- end post-wrap -->
			</div><!-- end posts-container -->
		</div><!-- end container -->
	</section>

<?php get_footer(); ?>