<?php
add_action('wp_ajax_load_more_posts', 'load_more_posts_callback');
add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts_callback');

function load_more_posts_callback() {
	
    $posts_numb = 3;
	$already_loaded = $_POST['alreadyLoaded'];
	$cat_id = $_POST['cat'];
	
	$args = array(
		'post_type'		 => 'post',
		'posts_per_page' => $posts_numb,
		'post_status'    => 'publish',
		'order'			 => 'DESC',
		'orderby'        => 'date',
		'offset' 		 => $already_loaded,
	);
	
	if ($cat_id != '') {
        $args['cat'] = $cat_id;
    }

	$get_posts = new WP_Query($args);
	$post_count = count($get_posts->posts);
	$total = $get_posts->found_posts;

	if ($get_posts->have_posts()) {
		$i = 0;
		while ($get_posts->have_posts()) : $get_posts->the_post();
			include(locate_template('template-parts/partials/blocks/news-block.php'));
		$i++;
		endwhile;
	}
	else { ?>
		<h3 class="no-more-posts">No posts found</h3>
	<?php
	}
	wp_reset_postdata();

	$posts_so_far = $already_loaded + $post_count;
	if ($post_count >= $posts_numb && $posts_so_far < $total) { 
		if ($posts_so_far != $total) { ?>
			<div class="load-more-posts">
				<a class="load-more button dark" href="#">Load More</a>
				<input type="hidden" class="post-count" value="<?php echo $posts_so_far; ?>" />
			</div>
		<?php
		}
	}
	   
die();
}
?>