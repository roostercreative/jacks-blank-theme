"use strict"

/*
========================
List Dependencies
========================
*/

var gulp = require("gulp")
var sass = require("gulp-sass")(require("sass"))
var postcss = require("gulp-postcss")
var concatCss = require("gulp-concat-css")
var autoprefixer = require("autoprefixer")
var cssnano = require("cssnano")
var sourcemaps = require("gulp-sourcemaps")
var order = require("gulp-order")
var terser = require("gulp-terser")
var concat = require("gulp-concat")
var imagemin = require("gulp-imagemin")
var browserSync = require("browser-sync").create()
var reload = browserSync.reload

var postcssProcessorsProd = [autoprefixer, cssnano()]

//Compile Sass
gulp.task("compileSass", function (done) {
	return gulp
		.src("src/sass/**/*.scss", "!src/sass/editor-style.scss")
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
		.pipe(postcss(postcssProcessorsProd))``
		.pipe(sourcemaps.write("."))
		.pipe(gulp.dest("dist/css"))
	done()
})

//Concatinate CSS Vendor files
gulp.task("concatCssVendor", function (done) {
	return gulp
		.src(["src/sass/vendor/*.scss", "src/sass/vendor/*.css"])
		.pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
		.pipe(concatCss("vendor.css"))
		.pipe(postcss(postcssProcessorsProd))
		.pipe(gulp.dest("dist/css/vendor"))
	done()
})

//Minify JS
gulp.task("minifyJS", function (done) {
	return gulp.src(["src/js/**/*.js", "!src/js/vendor/ignore/*.js"]).pipe(terser()).pipe(gulp.dest("dist/js"))
	done()
})

//Concatinate vendor JS
gulp.task("concatJS", function (done) {
	return gulp
		.src(["src/js/vendor/*.js"])
		.pipe(order(["modernizr-custom.js", "slick.min.js", "ofi.min.js", "jquery.ihavecookies.min.js"]))
		.pipe(terser())
		.pipe(concat("vendor.js"))
		.pipe(gulp.dest("dist/js/vendor"))
	done()
})

//Minify Images
gulp.task("minifyImages", function (done) {
	return gulp.src("src/images/**/*").pipe(imagemin()).pipe(gulp.dest("dist/images/"))
	done()
})

//Set up Browsersync
gulp.task("browser-sync", function (done) {
	browserSync.init({
		open: "external",
		host: "wp-testing.local",
		proxy: "wp-testing.local",
		watchOptions: {
			debounceDelay: 1000,
		},
	})
	gulp.watch(["**/*.php", "!node_modules/**/*.*"]).on("change", reload)
	gulp.watch("src/sass/**/*.scss").on("change", reload)
	gulp.watch("src/js/**/*.js").on("change", reload)
	done()
})

//Set up Watchers
function watchFiles() {
	gulp.watch("src/sass/**/*.scss", gulp.series("compileSass"))
	gulp.watch(["src/sass/vendor/*.scss", "src/sass/vendor/*.css"], gulp.series("concatCssVendor"))
	gulp.watch("src/js/**/*.js", gulp.series("minifyJS"))
	gulp.watch("src/js/vendor/*.js", gulp.series("concatJS"))
	gulp.watch("src/images/**/*", gulp.series("minifyImages"))
}

gulp.task("watch", gulp.parallel(watchFiles))
gulp.task("default", gulp.series("browser-sync", gulp.parallel("watch")))
