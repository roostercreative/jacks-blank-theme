<?php get_header(); ?>

    <section class="page-section basic-content" role="section" aria-label="Author information">
        <div class="container clearfix">
            <?php 
            //get author info
            $current_author = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>

            <h2>About: <?php echo $current_author->nickname; ?></h2>
            <div class="author-info" role="author information">
                <?php if ($current_author->user_description) { ?>
                    <p><strong>Profile:</strong> <?php echo $current_author->user_description; ?></p>
                <?php } ?>
            </div><!-- end author-info -->

            <br/><br/>

            <h3>Posts by <?php echo $current_author->nickname; ?>:</h3>
            
            <?php if (have_posts()) { ?>
                <ul class="author-posts">
                    <?php while (have_posts()) : the_post(); ?>
                        <li class="author-post">
                            <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                                <?php the_title(); ?>
                            </a>,
                            <?php the_time('d M Y'); ?> in <?php the_category('&');?>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php
            }
            else { ?>
                <p><?php _e('No posts by this author.'); ?></p>
            <?php } ?>
        </div><!-- end container -->	
    </section>    

<?php get_footer(); ?>