<?php
/*
====================================================================================================
 1. Security setup
====================================================================================================

Add additional security measures to WordPress and the theme.
*/

require_once('functions/security.php');

/*
====================================================================================================
 2. Backend
====================================================================================================

Remove/edit unnecessary core functionality/code that affects the backend.
*/

require_once('functions/back-end.php');

/*
====================================================================================================
 3. Frontend
====================================================================================================

Remove/edit unnecessary core functionality/code from the theme and its functions.
*/

require_once('functions/front-end.php');

/*
====================================================================================================
 4. Register post types
====================================================================================================

Create custom post types for specific website content and register any associated custom taxonomies
*/

require_once('functions/inc/post-types.php');

/*
====================================================================================================
 5. Register widget/sidebar areas
====================================================================================================

Create widget areas/dynamic sidebars.
*/

require_once('functions/inc/widgets.php');


/*
====================================================================================================
 6. Create custom shortcodes
====================================================================================================

Create custom shortcodes for blocks of website content
*/

require_once('functions/inc/shortcodes.php');


/*
====================================================================================================
 7. SHORTCODES BUTTON
====================================================================================================

Create tinymce shortcodes button.
*/

require_once('functions/inc/custom-tinymce-buttons.php');

/*
====================================================================================================
8. AJAX scripts
====================================================================================================

Load custom AJAX scripts to be handled by WordPress' AJAX engine
*/

//require_once('ajax/ajax-load-more-posts.php');


?>