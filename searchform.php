<div class="search-form clearfix" aria-label="site search">
    <form action="<?php echo home_url( '/' ); ?>" id="searchform" method="get">
        <input type="text" id="s" name="s" value="" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', '' ); ?>" x-webkit-speech="true" />
        <input type="submit" value="Search" id="searchsubmit" />
    </form>
</div><!-- end search-form -->