<?php
/*
ERROR 404 TEMPLATE
*/
?>

<?php get_header(); ?>

    <section class="page-section basic-content" role="section" aria-label="Error 404">
        <div class="container clearfix">
            <div class="content-wrap">
                <h3>Page Not Found. Please try another URL.</h3>
            </div><!-- end content-wrap -->
        </div><!-- end container -->
    </section>   
		
<?php get_footer(); ?>