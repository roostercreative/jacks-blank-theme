<?php get_header(); ?>

	<section class="page-section basic-content" role="section" aria-label="Media attachment">
		<div class="container clearfix">
            <h1>Media Attachment</h1>
            <hr />
            <?php
            if (have_posts()) {
                while (have_posts()) : the_post();
                ?>
                    <p class="intro-p"><?php echo basename($post->guid); ?></p>
                    <?php
                    if (wp_attachment_is_image($post->id)) {
                        $image = wp_get_attachment_image_src($post->id, 'full');
                        ?>
                        <a href="<?php echo wp_get_attachment_url($post->id); ?>" target="_blank" title="Open this image">
                            <img src="<?php echo $image[0]; ?>" width="<?php echo $image[1]; ?>" height="<?php echo $image[2]; ?>" alt="<?php echo basename($post->guid); ?>" />
                        </a>
                        <?php
                    }
                    else { ?>
                        <p>
                            <a href="<?php echo wp_get_attachment_url($post->ID); ?>" target="_blank" title="Download this attachment">Download this attachment</a>
                        </p>
                    <?php
                    }
                endwhile;
            }
            ?>
		</div><!-- end container -->
	</section>
		
<?php get_footer(); ?>