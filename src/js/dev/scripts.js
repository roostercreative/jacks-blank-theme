// JavaScript Document
$(document).ready(function() {
    
/*
=====================
BODY FADE IN
=====================
*/	
	
	setTimeout(function() {
		$('body').removeClass('fade-out');
	}, 320); 

	setTimeout(function() {
		$('body').addClass('loaded');
		$('#page-loader').fadeOut(900);
		$('.page-wrap').removeClass('fade-out');
	}, 1200);
	

/*
===============================
ADDITIONAL BODYCLASSES
===============================
*/
	
	if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
		$('html').addClass('safari');
	}
	
	//IE11
	if (navigator.userAgent.match(/Trident.*rv:11\./)) {
		$('body').addClass('ie11');
	}
	
    
/*
=========================
OBJECT FIT
=========================
*/
	
	$(function() { 
		objectFitImages('img.object-fit');
	});
    
    
/*
=====================
TEL LINKS
=====================
*/	

	//Disable on desktop (non touch)...
	$('.no-touch a.tel').click(function(e) {
		e.preventDefault();
	});	
    

/*
=====================
TABLE WRAPPER
=====================
*/

	$('table').wrap('<div class="table-wrap clearfix"></div>');
	
	
/*
======================
IFRAME WRAPPER
======================
*/
	
	$('.page-section iframe').wrap('<div class="iframe-container"><div class="iframe-wrap clearfix"></div></div>');
    
	
/*
===============================
FIXED HEADER
===============================
*/
    
    //add header class
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1) {
			$('.main-header').addClass('scrolled');
		} else {
			$('.main-header').removeClass('scrolled');
		}
	});
	
	//check page position and add class if not at the top of page on load
	var pagePostion = $(window).scrollTop();
	
	if (pagePostion > 1) {
		$('.main-header').addClass('scrolled');
	} 
	else {
	    $('.main-header').removeClass('scrolled');
	}	
	
	
/*
======================
HAMBURGER MENU
======================
*/
	
	
	//open menu
	$('.hamburger').click(function() {
		$(this).toggleClass('active');
		$('.main-menu').toggleClass('active');
	}); 
    
    $('.main-menu nav.mobile ul li.menu-item-has-children').prepend('<div class="menu-expand"><i class="far fa-chevron-down arrow"></i></div>');
	
	$('.main-menu nav.mobile .menu-expand').click(function() {
		$(this).toggleClass('active');
		$(this).parent('li').toggleClass('active');
		$(this).parent('li').find('> ul.sub-menu').slideToggle(600);
	});
    
    
/*
=============================
SLIDERS (SLICK SLIDER)
=============================
*/	

	$('.home-banner .carousel .slider-wrap').slick({
		infinite: true,
		autoplay: true,
		speed: 600,
		autoplaySpeed: 6000,
		slidesToShow: 1,
		adaptiveHeight: true,
		arrows: false,
		dots: true,
		fade: true,
		cssEase: 'linear'
	});

	$(window).resize(function() {		
		$('.home-banner .carousel .slider-wrap').slick('refresh');
	});

	$(window).on('orientationchange', function() {		
		$('.home-banner .carousel .slider-wrap').slick('reinit');
	});
    
    /*$('.slider-wrap .slides').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        adaptiveHeight: true,
        arrows: false
    });*/
    
   
/*
======================================
SMOOTH SCROLL
======================================
*/
	
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);				   
				return false;
			}
		});
	});
    

/*
=========================
UP ARROW
=========================
*/

	$('.up').on('click', function(event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
            }, 1000
		);
	}); 
    

/*
==========================
AJAX
==========================
*/
    
    //LOAD MORE POSTS
	$('.news-posts .posts-container').on('click', '.load-more', function(e) {
		e.preventDefault();
		
		var loaded = $('input.post-count').val();

		$.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'POST',
			data: {
				action: 'load_more_posts',
				already_loaded: loaded,
			},
			beforeSend: function() {
				$('.news-posts .posts-container .load-more-posts').fadeTo(600, 0, function() {
					$('.news-posts .posts-container .load-more-posts').html('<div class="load"></div>');
				});
				$('.news-posts .posts-container .load-more-posts').delay(300).fadeTo(400, 1);
			},
			success: function(data) {
				$('.news-posts .posts-container .load-more-posts').delay(1000).fadeOut(700, function() {
					$('.news-posts .posts-container .load-more-posts').remove();
					$('.news-posts .posts-container .posts-wrap').append(data);
				});
			}
		});
	});
    
    
}); //END document.ready