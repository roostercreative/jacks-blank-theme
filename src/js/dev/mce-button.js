/* CUSTOM SHOTCODES */
(function() {
	tinymce.PluginManager.add('my_mce_button', function(editor, url) {
		editor.addButton('my_mce_button', {
			text: 'Shortcodes',
			icon: false,
			type: 'menubutton',
			menu: [
				{
					text: 'Insert Shortcodes',
					menu: [
						{
							text: 'Spacer',
							onclick: function() {
								editor.insertContent('[spacer]');
							}
						},
						{
							text: 'Indented Text',
							onclick: function() {
								editor.insertContent('<p>[indent]Enter text...[/indent]</p>');
							}
						},
						{
							text: 'Two Columns',
							onclick: function() {
								editor.insertContent('<p>[two_col]Enter column one image/text...[/two_col]</p>');
								editor.insertContent('<p>[two_col_last]Enter column two text...[/two_col_last]</p>');
							}
						},
						{
							text: 'Mailto Email',
							onclick: function() {
								editor.insertContent('<p>[email]Enter email address...[/email]</p>');
							}
						}
					]
				}
			]
		});
	});
})();