<?php
/* TEXT BANNER */

$banner_text = (get_field('banner_text')) ? get_field('banner_text') : "<h1>".get_the_title()."</h1>";

if (is_404()) {
    $banner_text = "<h1>Error 404</h1>";
}
else if (is_category()) {
    $banner_text = "<h1>".single_cat_title("", false)."</h1>";
}
else if (is_search()) {
	$banner_text = "<h1>Search</h1>";
}
else if (is_author()) {
	$banner_text = "<h1>Author</h1>";
}
else if (is_archive()) {
	$post = $posts[0];
	if (is_day()) { $banner_text = "<h1>Archive for ".get_the_time('F jS, Y')."</h1>"; }
	else if (is_month()) { $banner_text = "<h1>Archive for ".get_the_time('F, Y')."</h1>"; }
	else if (is_year()) { $banner_text = "<h1>Archive for ".get_the_time('Y')."</h1>"; }
}

if ($banner_text) { ?>
    <section class="main-banner text-banner" role="section" aria-label="Banner">
        <div class="container">
            <div class="banner-content">
                <div class="content-wrap">
                    <?php echo $banner_text; ?>
                </div><!--end content-wrap -->
            </div><!-- end banner-content -->
        </div><!-- end container -->  
    </section>
<?php } ?>