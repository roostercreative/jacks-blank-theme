<script type="text/javascript">
	(function($) {
		if ($('.acf-map').length > 0) {
			function render_map($el) {

				//vars
				var $markers = $el.find('.marker');
				var args = {
					zoom: 12,
					scrollwheel: false,
					disableDefaultUI: false,
					streetViewControl: false,
					mapTypeControl: false,
					scaleControl: false,
					fullscreenControl: false,
					center: new google.maps.LatLng(0, 0),
					mapTypeId: google.maps.MapTypeId.ROADMAP,
				};	        	
				var map = new google.maps.Map($el[0], args);

				map.markers = []; //add a markers reference

				//add markers
				$markers.each(function() {
					add_marker($(this), map);
				});

				center_map(map); //center map

				//zoom and scroll on click
				google.maps.event.addListener(map, 'click', function(event) {
					this.setOptions({scrollwheel: true});
				});

				//disable again when leaving the map
				google.maps.event.addListener(map, 'mouseout', function(event) {
					this.setOptions({scrollwheel: false});  
				});

				//Reposition/centre the map on window resize
				google.maps.event.addDomListener(window, "resize", function() {
					var center = map.getCenter();
					google.maps.event.trigger(map, "resize");
					map.setCenter(center); 
				});
			}

			//add_marker function
			function add_marker($marker, map) {
				//var
				var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));
				var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
				
				//create marker
				var marker = new google.maps.Marker({
					position: latlng,
					map: map,
				});

				marker.addListener('mouseover', toggleBounce);
				function toggleBounce() {
					if (marker.getAnimation() !== null) {
						marker.setAnimation(null);
					} 
					else {
						marker.setAnimation(google.maps.Animation.BOUNCE);
						setTimeout(function(){ marker.setAnimation(null); }, 400);
					}
				}

				//add to array
				map.markers.push(marker);
			}

			//center_map function
			function center_map(map) {
				//vars
				var bounds = new google.maps.LatLngBounds();

				//loop through all markers and create bounds
				$.each(map.markers, function(i, marker) {
					var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
					bounds.extend(latlng);
				});

				//only 1 marker?
				if (map.markers.length == 1) {
					map.setCenter(bounds.getCenter()); //set center of map
				}
				else {
					map.fitBounds(bounds); //fit to bounds
				}
				map.setZoom(12);
			}
			
			//document ready function
			$(document).ready(function() {
				$('.acf-map').each(function() {
					render_map($(this));
				});
			});
			
		} //if acf-map is not empty
		
	})(jQuery);
</script>