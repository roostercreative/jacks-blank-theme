<?php
/* BREADCRUMB */
?>

<?php
if (!is_front_page()) {
	if (function_exists('yoast_breadcrumb')) { ?>
		<section class="breadcrumb" role="section" aria-label="Breadcrumb">
			<div class="container clearfix">
				<?php yoast_breadcrumb('<p id="breadcrumbs">','</p>'); ?>
			</div><!-- end container -->	
		</section>
	<?php
	}
}
?>