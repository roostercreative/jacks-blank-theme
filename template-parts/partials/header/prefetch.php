<?php
/* PREFETCH & PRECONNECT */
?>

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://www.gstatic.com">
<link rel="preconnect" href="https://kit.fontawesome.com">  
<link rel="dns-prefetch" href="https://kit.fontawesome.com">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://code.jquery.com">
<link rel="dns-prefetch" href="https://code.jquery.com">
<link rel="preconnect" href="//cdnjs.cloudflare.com">
<link rel="dns-prefetch" href="//cdnjs.cloudflare.com">
<link rel="preconnect" href="https://www.googletagmanager.com"> 
<link rel="dns-prefetch" href="https://www.googletagmanager.com">