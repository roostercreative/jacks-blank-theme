<?php
/* HAMBURGER */
?>

<div class="hamburger" title="Menu" aria-label="Hamburger">
    <span class="top"></span>
    <span class="middle"></span>
    <span class="bottom"></span>
</div><!-- end hamburger -->