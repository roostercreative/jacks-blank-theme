<?php
/*
MOBILE MENU
*/
?>

<div class="menu-overlay"></div>

<div class="mobile-menu" role="navigation" aria-label="Mobile navigation">
    <div class="menu-header">
        <div class="close" aria-label="Close button"></div>
    </div><!-- end menu-header -->
    <div class="menu-wrap">
        <nav class="menu">
            <?php wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'mobile-menu', 'container' => '', 'depth' => 2)); ?>
        </nav>
    </div><!-- end menu-wrap -->
</div><!-- end mobile-menu -->