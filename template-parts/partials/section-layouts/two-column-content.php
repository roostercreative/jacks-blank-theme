<?php
/*
TWO COLUMN CONTENT
*/

$left_col_content = get_sub_field('left_column_content', $id);
$right_col_content = get_sub_field('right_column_content', $id);
$container_width = get_sub_field('tcc_container_width', $id);
$padding_top = get_sub_field('tcc_padding_top', $id);
$padding_bottom = get_sub_field('tcc_padding_bottom', $id);
?>

<section class="page-section two-column-content two-columns white-bg<?php echo " ".esc_attr($padding_top); echo " ".esc_attr($padding_bottom); ?>" id="<?php echo esc_attr($section_id); ?>">
	<div class="container<?php echo " ".esc_attr($container_width); ?>">
		<div class="content-wrap grid-container wow fadeIn" data-wow-offset="100">
			<?php if ($left_col_content) { ?>
                <div class="col left">
                    <?php echo $left_col_content; ?>
                </div><!-- end col -->
            <?php
            }
            if ($right_col_content) { ?>
                <div class="col right">
                    <?php echo $right_col_content; ?>
                </div><!-- end col -->
            <?php } ?>
		</div><!-- end content-wrap -->
	</div><!-- end container -->
</section>