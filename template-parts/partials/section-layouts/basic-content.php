<?php
/* 
BASIC CONTENT 
*/

$content = get_sub_field('basic_content', $id);
$container_width = get_sub_field('bc_container_width', $id);
$padding_top = get_sub_field('bc_padding_top', $id);
$padding_bottom = get_sub_field('bc_padding_bottom', $id);
?>

<section class="page-section basic-content white-bg<?php echo " ".esc_attr($padding_top); echo " ".esc_attr($padding_bottom); ?>" id="<?php echo "section-"esc_attr($row_count); ?>">
	<div class="container<?php echo " ".esc_attr($container_width); ?>">
        <?php if ($content) { ?>
            <div class="content-wrap wow fadeIn" data-wow-offset="100">
                <?php echo $content; ?>
            </div><!-- end content-wrap -->
        <?php } ?>
	</div><!-- end container -->
</section>
