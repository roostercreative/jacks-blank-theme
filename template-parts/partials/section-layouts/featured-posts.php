<?php
/* 
FEATURED POSTS 
*/

$section_heading = get_sub_field('fp_section_heading', $id);
$post_type = get_sub_field('post_type_select', $id);
$padding_top = get_sub_field('fp_padding_top', $id);
$padding_bottom = get_sub_field('fp_padding_bottom', $id);
$container_width = get_sub_field('fp_container_width', $id);

$args = array(
    'post_type'		 => $post_type,
    'posts_per_page' => 3,
    'post_status'    => 'publish',
    'order'			 => 'DESC',
);

$get_posts = new WP_Query($args);
$post_count = count($get_posts->posts); 
$total = $get_posts->found_posts;

if ($get_posts->have_posts()) { ?>
    <section class="page-section featured-posts white-bg<?php echo " ".esc_attr($padding_top); echo " ".esc_attr($padding_bottom); ?>" id="<?php echo "section-"esc_attr($row_count); ?>">
        <div class="container<?php echo " ".esc_attr($container_width); ?> wow fadeIn" data-wow-offset="100">
            <?php if ($section_heading) { ?>
                <div class="section-heading">
                    <div class="content-wrap">
                        <?php echo $section_heading; ?>
                    </div><!-- end content-wrap -->
                </div><!-- end section-heading -->
            <?php } ?>
            <div class="posts-container three-columns">
                <div class="posts-wrap grid-container">
                    <?php
                    $i = 0;
                    while ($get_posts->have_posts()) : $get_posts->the_post();
                        if ($post_type == 'post') {
                            include(locate_template('template-parts/partials/blocks/news-block.php'));
                        }
                        else {
                            include(locate_template('template-parts/partials/blocks/news-block.php'));
                        }
                    $i++; 
                    endwhile; 
                    ?>
                </div><!-- end grid-container -->
            </div><!-- end posts -->
        </div><!-- end container -->    
    </section>    
<?php 
} 
wp_reset_postdata();
?>