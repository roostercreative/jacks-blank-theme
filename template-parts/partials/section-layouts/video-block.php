<?php
/* FULL WIDTH VIDEO BLOCK */

$video_type = get_sub_field('video_type');
$external_url = get_sub_field('external_url');
$mp4 = get_sub_field('video_mp4');
$poster_img = get_sub_field('video_poster_image');
$alt = $poster_img['alt'];
$container_width = (get_sub_field('video_container_width')) ? get_sub_field('video_container_width') : 'small';

if ($video_type == 'url') {
    $video = $external_url;
}
else {
    $video = $mp4['url'];
}

//output if video sources are set
if ($video) { ?>
    <section class="page-section video-block cover wow fadeIn" data-wow-offset="200" id="section-<?php if (isset($row_count)) { echo $row_count; } ?>" <?php if (isset($alt)) { echo "aria-label='".$alt."'"; } ?> role="section">
        <div class="container<?php if (isset($container_width)) { echo " ".$container_width; } ?>">
            <div class="video-wrapper clearfix">
                <?php echo do_shortcode('[video src="'.$video.'" poster="'.$poster_img['url'].'" preload="auto"]'); ?>
            </div><!-- end video wrapper -->
        </div><!-- end container -->
    </section>
<?php } ?>