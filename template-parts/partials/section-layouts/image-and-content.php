<?php 
/*
IMAGE AND CONTENT
*/

$section_heading = get_sub_field('section_heading', $id);
$container_width = get_sub_field('container_width', $id);
$padding_top = get_sub_field('iac_padding_top', $id);
$padding_bottom = get_sub_field('iac_padding_bottom', $id);

if (have_rows('image_and_content')) { ?>
    <section class="page-section image-and-content white-bg<?php echo " ".esc_attr($padding_top); echo " ".esc_attr($padding_bottom); ?>" id="<?php echo "section-"esc_attr($row_count); ?>">
        <div class="container<?php echo " ".esc_html($container_width); ?>">
            <?php if ($section_heading) { ?>
                <div class="section-heading wow fadeIn">
                    <div class="content-wrap">
                        <?php echo $section_heading; ?>
                    </div><!-- content-wrap -->
                </div><!-- end section-heading -->
            <?php } ?>
            <div class="items-wrap clearfix">
                <?php
                while (have_rows('image_and_content')) : the_row();
                    $section_image = get_sub_field('image');
                    $section_content = get_sub_field('content');
                ?>
                    <div class="section-item grid-container wow fadeIn" data-wow-offset="100">
                        <?php if ($section_image) { ?>
                            <div class="section-image col">
                                <img loading="lazy" <?php responsive_image($section_image['id'], 'featured', '1200px', '50vw'); ?> alt="<?php echo esc_attr($section_image['alt']); ?>" />
                            </div><!-- end section-image -->
                        <?php } ?>
                        <?php if ($section_content) { ?>
                            <div class="section-content col">
                                <div class="content">
                                    <div class="content-wrap">
                                        <?php echo $section_content; ?>
                                    </div><!-- end content-wrap -->
                                </div><!-- end content -->
                            </div><!-- end section-content -->
                        <?php } ?>
                    </div><!-- end flex-container -->
                <?php endwhile; ?>
            </div><!-- end items-wrap -->   
        </div><!-- end container -->
    </section>
<?php } ?>    