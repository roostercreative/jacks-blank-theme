<?php
/* CAROUSEL CONTROLS */
?>

<div class="carousel-nav" id="carousel-nav" data-navid="<?php echo uniqid(); ?>" role="carousel controls">
    <div class="prev" data-controls="prev">
        <i class="far fa-chevron-left"></i>
    </div>
    <div class="next" data-controls="next">
        <i class="far fa-chevron-right"></i>     
    </div>
</div><!-- end controls -->