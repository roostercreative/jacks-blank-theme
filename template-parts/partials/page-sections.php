<?php
/* 
PAGE SECTIONS 
*/

if (is_tax()) {
	$id = get_queried_object()->term_id;
}
else {
	$id = get_the_id();
}

if (have_rows('page_sections')) {
	
	$row_count = 1; //use count for row ID (section-*)
	while (have_rows('page_sections')) : the_row();

		//BASIC CONTENT LAYOUT
		if (get_row_layout() == 'basic_content') { 
			include(locate_template('template-parts/partials/section-layouts/basic-content.php'));
		}

        //TWO COLUMN CONTENT LAYOUT
		if (get_row_layout() == 'two_column_content') { 
			include(locate_template('template-parts/partials/section-layouts/two-column-content.php'));
		}

        //IMAGE AND CONTENT LAYOUT
		if (get_row_layout() == 'image_and_content') { 
			include(locate_template('template-parts/partials/section-layouts/image-and-content.php'));
		}

        //FEATURED POSTS LAYOUT
		if (get_row_layout() == 'featured_posts') { 
			include(locate_template('template-parts/partials/section-layouts/featured-posts.php'));
		}

		//VIDEO BLOCK LAYOUT
		if (get_row_layout() == 'video_block') {
			include(locate_template('template-parts/partials/section-layouts/video-block.php'));
		}
	
	$row_count++;
	endwhile;

}
?>