<?php
/*
BLOG CATEGORIES 
*/

if (is_category()) {
    $currentcat = get_category(get_query_var('cat'));
}
$categories = get_categories(array(
    'orderby' => 'name',
    'parent'  => 0,
    'hide_empty' => 0,
    'exclude' => 1,
));

if ($categories) { ?>
    <section class="news-cats">
        <div class="category-links">
            <ul>
                <li class="link cat">
                    <a href="<?php echo esc_url(get_page_link(22)); ?>">View All</a>
                </li>
                <?php
                foreach ($categories as $cat) { 
                    $cat_link = get_term_link($cat->cat_ID);
                ?>
                    <li class="link cat<?php if (isset($currentcat) && $currentcat->cat_ID == $cat->cat_ID) { echo " current-cat"; } ?>">
                        <a href="<?php echo esc_url($cat_link); ?>" title="<?php echo esc_attr($cat->name); ?>"><?php echo $cat->name; ?></a>
                    </li>
                <?php } ?>	
            </ul>
        </div><!-- end news-cats -->
    </section>    
<?php } ?>