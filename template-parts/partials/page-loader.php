<?php
/* PAGE LOADER */
?>

<div class="page-loader" id="page-loader">
    <div class="loading-screen">
        <div class="logo">
            <img src="<?php echo get_bloginfo('template_directory'); ?>/src/images/logo.svg" loading="lazy" alt="<?php echo get_bloginfo('name'); ?>" />
        </div><!-- end logo -->
    </div><!-- end loading-screen -->  
</div><!-- end page-loader -->