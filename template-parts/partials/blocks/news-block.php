<?php
/*
NEWS BLOCK
*/

$block_id = get_the_ID();
$block_image = get_field('featured_image', $block_id);
?>

<article <?php post_class('news-block block') ?> id="post-<?php echo esc_attr($block_id); ?>">
    <?php if ($block_image) { ?>
        <div class="block-image">
            <a href="<?php the_permalink(); ?>">
                <img <?php responsive_image($block_image['id'], 'featured', '800px', '50vw'); ?> alt="<?php echo esc_attr($block_image['alt']); ?>" class="gallery-image" />
                <div class="overlay"><span>Read more</span></div>
            </a>
        </div><!-- end image -->
    <?php } ?>
    <div class="block-content">
        <h3><a href="<?php the_permalink($block_id); ?>"><?php the_title(); ?></a></h3>
        <div class="excerpt">
            <?php the_field('the_excerpt', $block_id); ?>
            <hr />
        </div><!-- end excerpt -->
        <div class="post-meta">
            <p>Published <?php the_time('d/m/y', $block_id); ?></p>
            <?php //post_read_time() ?>
        </div><!-- end post-meta -->
        <a class="button" href="<?php the_permalink($block_id); ?>">Read more</a>
    </div><!-- end block-content -->
</article>