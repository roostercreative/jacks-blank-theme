<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
        <section class="page-section basic-content" role="section" id="section-1">
            <div class="container clearfix">
                <div class="post-meta">
                    <p class="post-date">Posted: <?php the_time('d/m/y'); ?></p>
                </div><!-- end post-meta -->
                <div class="content">
                    <?php the_content(); ?>
                </div><!-- end content -->
            </div><!-- end container -->
        </section>
		
	<?php endwhile; endif; ?>

<?php get_footer(); ?>