<?php get_header(); ?>

	<section class="page-section news-posts four-columns" id="section-0" role="section">
		<div class="container clearfix">
			<?php //get_template_part('template-parts/partials/posts/news-cats'); ?>
			<div class="posts-container blocks-container wow fadeIn" data-wow-offset="200">
				<div class="posts-wrap grid-container">
					<?php
					$cat_id = get_query_var('cat');
					$posts_numb = 16;
					
					$args = array(
						'post_type'		 => 'post',
						'posts_per_page' => $posts_numb,
						'post_status'    => 'publish',
						'order'			 => 'DESC',
						'orderby'        => 'date',
						'cat' 			 => $cat_id
					);
					$get_posts = new WP_Query($args);
					$post_count = count($get_posts->posts);
					$total = $get_posts->found_posts;

					if ($get_posts->have_posts()) {
						$i = 1;
						while ($get_posts->have_posts()) : $get_posts->the_post();
							include(locate_template('template-parts/partials/blocks/post-block.php'));
						$i++;
						endwhile;
					}
					else { ?>
						<h3 class="no-more-posts">No posts found</h3>
					<?php
					}
					wp_reset_postdata();
					
					if ($post_count >= $posts_numb) { ?>
						<div class="load-more-posts">
							<a class="load-more button" href="#">Load More</a>
							<input type="hidden" class="post-count" value="<?php echo $post_count; ?>" />
							<input type="hidden" class="cat-id" value="<?php if ($cat_id) { echo $cat_id; } ?>" />
						</div><!-- end load-more-posts -->
					<?php } ?>
				</div><!-- end post-wrap -->
			</div><!-- end posts-container -->
		</div><!-- end container -->
	</section>

<?php get_footer(); ?>