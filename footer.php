    </main>

    <footer class="main-footer" role="footer">
    	<div class="sign-off">
            <p class="copy">&copy; <?php echo date('Y'); ?></p>
            <p class="rooster">Site designed by <a href="https://www.roostermarketing.com" target="_blank" title="Rooster Marketing">Rooster Marketing</a></p>
        </div><!-- end sign-off -->
        <a class="up" href="#" title="Back Up"><i class="up-arrow"></i></a>
    </footer>

	<?php wp_footer(); ?>
	
	<!-- SCRIPTS -->
    <?php if (is_page_template('templates/page-contact.php')) { ?>
	<!--<script defer="defer" type="text/javascript" src="https://maps.google.com/maps/api/js?key=<?php //echo get_google_apikey(); ?>&libraries=geometry,places"></script>-->
    <?php } ?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/dist/js/vendor/vendor.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/dist/js/dev/scripts.js"></script>

    <script>
        if (navigator.appVersion.indexOf("MSIE 10") !== -1 || !!window.MSInputMethodContext && !!document.documentMode) {
            // do nothing in IE10 & 11 only
        }
        else {
            loadJS("<?php bloginfo('template_url'); ?>/src/js/vendor/ignore/set-window-height.js");
        }
    </script>
    
    <!-- IE10 NOTICE -->
    <!--[if lte IE 10]>
        <div class="ie10notice">
            <div class="container">
                <img src="<?php bloginfo('template_url'); ?>/images/ie-logo.png" loading="lazy" alt="Internet Explorer" />
                <h3>Important Notice</h3>
                <p>You are using an <strong>out of date</strong> version of Internet Explorer. This browser may not display all features of this website.</br>Please <a href="http://windows.microsoft.com/en-GB/internet-explorer/download-ie" target="_blank">click here</a> to upgrade.</p>
            </div>
        </div>
    <![endif]-->
	
    <script defer="defer" src="//instant.page/5.1.0" type="module" integrity="sha384-by67kQnR+pyfy8yWP4kPO12fHKRLHZPfEsiSXR8u2IKcTdxD805MGUXBzVPnkLHw"></script>
</body>
</html>