<?php
/*
Template Name: News Page
*/
?>

<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
		<section class="page-section news-posts three-columns" role="section" aria-label="News posts" id="section-0">
            <div class="container">
                <div class="posts-container blocks-container">
                    <div class="posts-wrap grid-container">
                        <?php
                        $posts_numb = 6;
                        $args = array(
                            'post_type'		 => 'post',
                            'posts_per_page' => $posts_numb,
                            'post_status'    => 'publish',
                            'order'			 => 'DESC',
                        );
                        $get_posts = new WP_Query($args);
                        $post_count = count($get_posts->posts);

                        if ($get_posts->have_posts()) {
                            $i = 0;
                            while ($get_posts->have_posts()) : $get_posts->the_post(); 
                                include(locate_template('template-parts/partials/blocks/news-block.php'));
                            $i++;
                            endwhile;
                        } 
                        wp_reset_postdata();
                        
                        if ($post_count >= 6) { ?>
                            <div class="load-more-posts">
                                <a class="load-more" href="#">Load More</a>
                                <input type="hidden" class="post-count" value="<?php echo $post_count; ?>" />
                            </div><!-- end load-more-posts -->
                        <?php } ?>
                    </div><!-- end post-wrap -->
                </div><!-- end posts-container -->
            </div><!-- end container -->
        </section>

        <?php get_template_part('template-parts/partials/page-sections'); ?>
		
	<?php endwhile; endif; ?>                           
                            
<?php get_footer(); ?>