<?php
/* 
===========================
CUSTOM SHORTCODES 
===========================
*/

//SPACER SHORTCODE
function spacer($atts, $content, $tag) {
	$output = '';
	$output = '<div class="spacer clearfix"></div>';
	return $output;
}
add_shortcode('spacer', 'spacer'); /*[spacer]*/


//TWO COL SHORTCODE
function two_col($atts, $content, $tag) {
	$output = '';
	$content = do_shortcode(shortcode_unautop($content));
    $content = preg_replace('#^<\/p>|^<br \/>|<p>$#', '', $content);
	$output = '<div class="two-col clearfix">'.$content.'</div>';
	return $output;
}
add_shortcode('two_col', 'two_col'); /*[two_col][/two_col]*/

function two_col_last($atts, $content, $tag) {
	$output = '';
	$content = do_shortcode(shortcode_unautop($content));
    $content = preg_replace('#^<\/p>|^<br \/>|<p>$#', '', $content);
	$output = '<div class="two-col last clearfix">'.$content.'</div><div class="clear"></div>';
	return $output;
}
add_shortcode('two_col_last', 'two_col_last'); /*[two_col_last][/two_col_last]*/


//INDENT WRAPPER SHORTCODE
function indent($atts, $content, $tag) {
	$output = '';
	
	$content = do_shortcode(shortcode_unautop($content));
    $content = preg_replace('#^<\/p>|^<br \/>|<p>$#', '', $content);
	
	$output = '<div class="indent clearfix">'.$content.'</div>';
	return $output;
}
add_shortcode('indent', 'indent'); /*[indent][/indent]*/


//ENCRYPT MAILTO LINKS
function wpcodex_hide_email_shortcode($atts, $content = null) {
	if (!is_email($content)) {
		return;
	}
	$content = antispambot($content);
	$email_link = sprintf('mailto:%s', $content);

	return sprintf('<a href="%s">%s</a>', esc_url($email_link, array('mailto')), esc_html($content));
}
add_shortcode('email', 'wpcodex_hide_email_shortcode'); //[email]johndoe@email.com[/email]
?>