<?php
/* 
==============================
WIDGET AREAS & FUNCTIONS
==============================
*/

// Declare sidebar widget zone
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Sidebar Widgets',
		'id'   => 'sidebar-widgets',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}

//WIDGET TITLES
function remove_widget_title($widget_title) {
	if (substr($widget_title, 0, 1) == '!')
		return;
	else 
		return ($widget_title);
}
add_filter('widget_title', 'remove_widget_title');


//ALLOW SHORTCODES IN WIDGETS
add_filter('widget_text', 'do_shortcode');
?>