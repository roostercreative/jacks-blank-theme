<?php
/* 
============================
BACK END FUNCTIONS 
============================
*/

// MOVE YOAST TO BOTTOM
function yoasttobottom() {
	return 'low';
}
add_filter('wpseo_metabox_prio', 'yoasttobottom');


//REMOVE LINK TO IMAGE AS DEFAULT
function image_display_settings() {
	update_option('image_default_align', 'none');
	update_option('image_default_size', 'full');
	update_option('image_default_link_type', 'none');
}
add_action('admin_init', 'image_display_settings', 10);


//Add SVG capabilities
function wpcontent_svg_mime_type($mimes = array()) {
    $mimes['svg']  = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'wpcontent_svg_mime_type');


//restrict image size on upload
function validate_image_size($file) {
    $image = getimagesize($file['tmp_name']);
    $maximum = array('width' => '3000', 'height' => '3000');
    
	$image_width = $image[0];
    $image_height = $image[1];

    $too_large = "Image dimensions are too large. Maximum size is {$maximum['width']} by {$maximum['height']} pixels. Uploaded image is $image_width by $image_height pixels.";

    if ($image_width > $maximum['width'] || $image_height > $maximum['height']) {
        $file['error'] = $too_large; 
        return $file;
    }
    else {
        return $file;
	}
}
add_filter('wp_handle_upload_prefilter', 'validate_image_size');


//ALLOW SHORTCODES IN CF7
function mycustom_wpcf7_form_elements($form) {
	$form = do_shortcode($form);
	return $form;
}
add_filter('wpcf7_form_elements', 'mycustom_wpcf7_form_elements');


//Remove <p> tags that wrap shortcodes in WYSIWYG content
function remove_p_shortcode_format($content) {
	$content = preg_replace('/(<p>)\s*(<div)/','<div',$content);
	$content =  preg_replace('/(<\/div>)\s*(<\/p>)/', '</div>', $content);
	return $content;
}
add_filter('the_content','remove_p_shortcode_format', 11);


//Remove <p> tags that wrap images in WYSIWYG content
function remove_p_tags_from_images($content) {
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'remove_p_tags_from_images');
add_filter('acf_the_content', 'remove_p_tags_from_images');


//OPTIONS PAGES
/*if( function_exists('acf_set_options_page_title') ) {
 	acf_set_options_page_title( __('Site Options') );
}

if( function_exists('acf_add_options_sub_page') ) {
	acf_add_options_sub_page( 'General' );
 	acf_add_options_sub_page( 'Footer Area' );
}*/

//CUSTOM EDITOR STYLES
function cd_add_editor_styles() {
    add_editor_style("dist/css/editor-style.css");
}
add_action('admin_init', 'cd_add_editor_styles');


//STYLE SELECT EDITOR TO ALLOW CUSTOM STYLES
function themeit_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect', 'fontsizeselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'themeit_mce_buttons_2');

function themeit_tiny_mce_before_init($settings) {
    $settings['theme_advanced_blockformats'] = 'p,a,div,span,h1,h2,h3,h4,h5,h6,tr,hr,';
    $style_formats = array(
        array('title' => 'Span', 'block' => 'span',  'classes' => '', 'wrapper' => true),
        array('title' => 'Div', 'block' => 'div',  'classes' => '', 'wrapper' => true),
        array('title' => 'Button', 'selector' => 'a', 'classes' => 'button'),
    );
    $settings['style_formats'] = json_encode($style_formats);
    return $settings;
}
add_filter('tiny_mce_before_init', 'themeit_tiny_mce_before_init');


//SEARCH BY ID
new WPSearchById();
class WPSearchById {
	function __construct() {
		add_filter('posts_where', array(&$this, 'posts_where'));
	}
	function posts_where($where) {
		if (is_admin() && is_search()) {
			$s = $_GET['s'];
			if (!empty($s)) {
				if (is_numeric($s)) {
					global $wpdb;
					$where = str_replace('(' . $wpdb->posts . '.post_title LIKE', '(' . $wpdb->posts . '.ID = ' . $s . ') OR (' . $wpdb->posts . '.post_title LIKE', $where);
				}
				else if (preg_match("/^(\d+)(,\s*\d+)*\$/", $s)) {
					global $wpdb;
					$where = str_replace('(' . $wpdb->posts . '.post_title LIKE', '(' . $wpdb->posts . '.ID in (' . $s . ')) OR (' . $wpdb->posts . '.post_title LIKE', $where);
				}
			}
		}
		return $where;
	}
}


//HIDE UNUSED AREAS - ADMIN PANEL 
function remove_menus() { 
	remove_menu_page('edit-comments.php');  
}
add_action('admin_menu', 'remove_menus');


if (get_current_user_id() !== 1) {
	function remove_acf_menu_pages() {
		// Remove the default acf link.
		remove_menu_page('edit.php?post_type=acf-field-group');
	}
	add_action('admin_init', 'remove_acf_menu_pages');
}


//ACF FIELDS SAVE
 function my_acf_json_save_point($path) {
    $path = get_stylesheet_directory().'/functions/acf-json'; //update path
    return $path;  
}
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

//ACF FIELDS LOAD
function my_acf_json_load_point($paths) {
    unset($paths[0]); //remove original path (optional)
    $paths[] = get_stylesheet_directory().'/functions/acf-json'; //append path
    return $paths;
}
add_filter('acf/settings/load_json', 'my_acf_json_load_point');


//remove user gravatar
function fc_remove_profile_image() {
    echo '<script>
    jQuery(document).ready(function() {
        jQuery("tr.user-profile-picture").remove();
        jQuery("td.column-username > img").remove();
    });
    </script>';

}
add_action('admin_head','fc_remove_profile_image');


//CUSTOM ADMIN STYLES 
function lh_acf_admin_head() { ?>
	<style type="text/css">
		.small-wysiwyg iframe {
            height: 150px !important;
            min-height: 150px !important;
        }
    </style>
<?php
}
add_action('acf/input/admin_head', 'lh_acf_admin_head');
?>