<?php
/* 
======================
WP SECURITY
======================
*/

//REMOVE WP VERSION
function remove_version_from_head() { 
	return '';
} 
add_filter('the_generator', 'remove_version_from_head');


//LOGIN ERRORS - MAKE BRUTE FORCE ATTACKS MORE DIFFICULT
function failed_login() {
    return 'the login information you have entered is incorrect.';
}
add_filter ('login_errors', 'failed_login');


//DISABLE XML-RPC PINGBACKS
function remove_x_pingback($headers) {
    unset($headers['X-Pingback']);
    return $headers;
}
add_filter('wp_headers', 'remove_x_pingback');
?>