<?php
/* 
===========================
FRONT END FUNCTIONS 
===========================
*/

//Async scripts to improve performance
if (!function_exists('uncoverwp_async_scripts')) {
    function uncoverwp_async_scripts($url) {
        if (strpos($url, '#asyncload') === false) {
            return $url;
        }
        else if (is_admin()) {
            return str_replace('?#asyncload', '', $url);
        }
        else {
    	    return str_replace('?#asyncload', '', $url)."' async='async";
        }
    }
    add_filter('clean_url', 'uncoverwp_async_scripts', 11, 1);
}


//Remove jQuery/jQuery Migrate (we'll use our own version from a CDN)	
function load_jquery() {
	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_deregister_script('jquery-migrate');
        wp_register_script('jquery', ''.get_template_directory_uri().'/src/js/vendor/ignore/jquery.min.js', array(), '3.5.1');
		wp_register_script('jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.1.0.min.js#asyncload', array('jquery'), '3.1.0', true);
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-migrate');
	}
}
add_action('template_redirect', 'load_jquery');


// Fully Disable Gutenberg editor.
function remove_block_css() {
    wp_dequeue_style('wp-block-library'); //Wordpress core
    wp_dequeue_style('wp-block-library-theme'); //Wordpress core
    wp_dequeue_style('wc-block-style'); //WooCommerce
    wp_dequeue_style('storefront-gutenberg-blocks'); //Storefront theme
}
add_filter('use_block_editor_for_post_type', '__return_false', 10);
add_action('wp_enqueue_scripts', 'remove_block_css', 100);


//defer cf7 scripts
add_filter('script_loader_tag', function($tag, $handle) {
    if ('contact-form-7' !== $handle)
        return $tag;

    return str_replace(' src', ' defer="defer" src', $tag);
}, 10, 2);


//font display swap
add_filter('generate_google_font_display', function() {
    return 'swap';
});


//defer recapture scripts
function add_defer_attribute($tag, $handle) {
	// add script handles to the array below
	$scripts_to_defer = array('google-recaptcha', 'wpcf7-recaptcha');
	foreach ($scripts_to_defer as $defer_script) {
		if ($defer_script === $handle) {
			return str_replace(' src', ' defer="defer" src', $tag);
		}
	}
	return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);


//dequeue recaptcha and host script
add_action('wp_print_scripts', function() {
    wp_dequeue_script('google-recaptcha');
});

function cf7_defer_recaptcha() {
    wp_enqueue_script('cf7recap', get_template_directory_uri().'src/js/vendor/ignore/recaptcha.js', array('jquery'), '1.0');
}
add_action('get_footer', 'cf7_defer_recaptcha');


//HIDE ADMIN BAR
add_filter('show_admin_bar', '__return_false');


//Remove WordPress version from stylesheets/scripts query strings
function remove_wp_ver_css_js($src) {
	if (strpos($src, 'ver=')) {
		$src = remove_query_arg('ver', $src);
	}
	return $src;
}


// Clean up the <head>
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');


//ACTIVE MENU CLASSES
function special_nav_class($classes, $item) {
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);


//MENU LEVELS FOR MAIN MENUS
function my_menu_class($menu) {
    $level = 0;
    $stack = array('0');
    foreach($menu as $key => $item) {
        while($item->menu_item_parent != array_pop($stack)) {
            $level--;
        }   
        $level++;
        $stack[] = $item->menu_item_parent;
        $stack[] = $item->ID;
        $menu[$key]->classes[] = 'level-'. ($level - 1);
    }                    
    return $menu;        
}
add_filter('wp_nav_menu_objects' , 'my_menu_class');


//CUSTOM IMAGE SIZES
add_image_size('sml_square', 250, 250, array('center', 'center'));
add_image_size('featured', 1200, 1200, array('center', 'center'));
add_image_size('featured_sml', 800, 800, array('center', 'center'));
add_image_size('3.2', 900, 600, array('center', 'center'));
add_image_size('full', 2200, 9999, array('center', 'center'));
add_image_size('banner_sml', 1100, 9999, false);

//RESPONSIVE IMAGES
function responsive_image_light($large_url, $small_url, $alt, $class = Null) {
    return '<img class="'.esc_attr($class).'" src="'.esc_url($large_url).'" srcset="'.esc_url($small_url).' 1000w,' .esc_url($large_url).' 2200w" sizes="100vw" loading="lazy" alt="'.esc_attr($alt).'" />';
}

function responsive_image($image_id, $image_size, $max_width, $screen_width = null) {
	// check the image ID is not blank.
	if ($image_id != '') { 
		// set the default src image size.
		$image_src = wp_get_attachment_image_url($image_id, $image_size);

		// set the srcset with various image sizes.
		$image_srcset = wp_get_attachment_image_srcset($image_id, $image_size);

		// set the default image metadata.
		$image_meta = wp_get_attachment_metadata($image_id);

		if ($screen_width != '') {
			$viewport_width = $screen_width;
		} else {
			$viewport_width = '100vw';
		}
		// generate the markup for the responsive image.
		echo 'src="'.esc_attr($image_src).'" srcset="'.esc_attr($image_srcset).'" loading="lazy" sizes="(max-width: '.esc_attr($max_width).') '.esc_html( $viewport_width).', '.esc_attr($max_width).'"';
	}
}


//SEARCH RESULTS - ONLY ALLOW CERTAIN POST TYPES TO SHOW IN THE MAIN WP SEARCH
function searchfilter($query) {
    if ($query->is_search && !is_admin()) {
		$query->set('post_type',array('post', 'page'));
	}
	return $query;
}
add_filter('pre_get_posts','searchfilter');


//GOOGLE MAPS API
function get_google_apikey() {
	return 'AIzaSyBKSmXoBDv8wAWZB21PEXyyl-_XJL0dars';
};

function my_acf_init() {
    acf_update_setting('google_api_key', get_google_apikey());
}
add_action('acf/init', 'my_acf_init');


//Exclude images from search results - WordPress
function exclude_images_from_search_results() {
	global $wp_post_types;
	$wp_post_types['attachment']->exclude_from_search = true;
}
add_action('init', 'exclude_images_from_search_results');


//NUMBERED PAGINATION
function pagination($custom_query) {
    $total_pages = $custom_query->max_num_pages;
    $big = 999999999;
    if ($total_pages > 1) {
        $current_page = max(1, get_query_var('paged'));
        echo paginate_links(array(
            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
            'next_text' => 'Next',
            'prev_text' => 'Previous',
            'type'=>'list',
        ));
    }
}


//ADD TEXT LINK CLASS TO PAGINATION
function posts_link_attributes() {
    return 'class="button"';
}
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');


// Custom walker to add a wrapper <div> around sub-menu <ul>'s
class child_wrap extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<div class=\"sub-wrap clearfix\"><ul class=\"sub-menu\">\n";
	}
	function end_lvl(&$output, $depth = 0, $args = array()) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul></div>\n";
	}
}


//PASSWORD PROTECT PLACEHOLDER
function password_protected_placeholder($output) {
    $placeholder = 'Enter password';
    $search = 'type="password"';
    return str_replace($search, $search . " placeholder=\"$placeholder\"", $output);
}
add_filter('the_password_form', 'password_protected_placeholder');


//Pretty printing debugging tool
function printr($var) {
	print '<pre>';
	print_r($var);
	print '</pre>';
}

//PRINT SVG
function print_svg_file($svg_url) {
    echo file_get_contents($svg_url);
}
?>