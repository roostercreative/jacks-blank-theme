<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<!-- META -->
	<meta charset="<?php bloginfo('charset'); ?>" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- PREFETCH -->
    <?php get_template_part('template-parts/partials/header/prefetch.php'); ?>

	<title><?php wp_title('');?></title>
    
    <!-- FONTS -->
    <script defer="defer" src="https://kit.fontawesome.com/5d1981e9cc.js" crossorigin="anonymous"></script>
	
    <!-- STYLES -->
    <?php $csstime = date ("Y-m-d\TH-i", filemtime(get_stylesheet_directory().'/dist/css/styles.css')); ?>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/dist/css/styles.css<?php echo '?'.$csstime; ?>">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/dist/css/vendor/vendor.css">
    
    <!-- ICONS -->
    <!--<link rel="apple-touch-icon" sizes="180x180" href="<?php //bloginfo('template_url'); ?>/dist/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php //bloginfo('template_url'); ?>/dist/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php //bloginfo('template_url'); ?>/dist/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php //bloginfo('template_url'); ?>/dist/images/favicons/site.webmanifest">
    <link rel="mask-icon" href="<?php //bloginfo('template_url'); ?>/dist/images/favicons/safari-pinned-tab.svg" color="#4f748a">
    <meta name="msapplication-TileColor" content="#4f748a">
    <meta name="theme-color" content="#ffffff">-->

	<?php wp_head(); ?>

    <script defer="defer" type="text/javascript">
        var cookieOptions = {
            title: '',
            message: 'Please accept cookies for optimal performance. For more information',
            delay: 600,
            expires: 365,
            link: '<?php echo get_home_url(); ?>/privacy-policy',
            onAccept: function() {
                var myPreferences = $.fn.ihavecookies.cookie();
                //console.log(myPreferences);
                if (typeof myPreferences === 'function') {
                    myPreferences();
                }
            },
            cookieTypes: [],
            uncheckBoxes: false,
            acceptBtnLabel: 'Accept Cookies',
            moreInfoLabel: 'Click Here',
            cookieTypesTitle: '',
            fixedCookieTypeLabel: '',
            fixedCookieTypeDesc: ''
        };

		$(document).ready(function() {
            function myPreferences() {
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-5SCXLCN');
            }

            if (typeof myPreferences === 'function' && $.fn.ihavecookies.cookie()) {
                // Load marketingscripts if it exists and the cookies have been accepted
                myPreferences();
            } 
            else if (typeof myPreferences === 'function') {
                // Call the cookie script if there is a marketingscripts function and the cookies haven't been accepted
                $('body').ihavecookies(cookieOptions);
            }
		});
    </script>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    
    <script>document.body.className += ' fade-out';</script>
	
    <header class="main-header" role="header">
        <div class="logo">
            <a href="<?php echo home_url('/'); ?>" title="<?php echo get_bloginfo('name'); ?>">
                <img src="<?php echo get_bloginfo('template_directory'); ?>/src/images/logo.svg" loading="lazy" alt="<?php echo get_bloginfo('name'); ?>" />
            </a>
        </div><!-- end logo -->
        <nav class="main-menu" role="navigation">
            <?php wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => '', 'container' => '', 'depth' => 2)); ?>
        </nav>
        <?php get_template_part('template-parts/partials/header/hamburger'); ?>
	</header>
    
    <?php get_template_part('template-parts/partials/header/mobile-menu'); ?>
    
    <main id="site-content" role="main">